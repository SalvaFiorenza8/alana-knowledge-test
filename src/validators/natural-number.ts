import { FormControl } from '@angular/forms';

export class NaturalNumberValidator {

  static isValid(control: FormControl): any {

    let value = Number(control.value);

    if(!Number.isInteger(value) || value < 1){
      return ({naturalNumber: true});
    }

    return (null);
  }

}
