import { FormGroup } from '@angular/forms';

export class GreaterThanValidator {

  static isValid(startKey: string, endKey: string) {
    return (group: FormGroup): any => {
      let start = Number(group.controls[startKey].value);
      let end = Number(group.controls[endKey].value);

      if (start >= end) {
        return ({greaterThan: true});
      }

      return (null);
    }
  }

}
