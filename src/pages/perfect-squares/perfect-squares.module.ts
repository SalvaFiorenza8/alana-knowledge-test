import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {PerfectSquaresPage} from './perfect-squares';

@NgModule({
  declarations: [
    PerfectSquaresPage,
  ],
  imports: [
    IonicPageModule.forChild(PerfectSquaresPage),
  ],
})
export class PerfectSquaresPageModule {
}
