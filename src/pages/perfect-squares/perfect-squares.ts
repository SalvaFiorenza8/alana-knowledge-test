import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PerfectSquaresLocalProvider} from '../../providers/perfect-squares-local/perfect-squares-local';
import {NaturalNumberValidator} from '../../validators/natural-number';
import {PerfectSquare} from '../../interfaces/perfect-square';
import {GreaterThanValidator} from '../../validators/greater-than';
import {NumberValidator} from '../../validators/number';

/**
 * Generated class for the PerfectSquaresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfect-squares',
  templateUrl: 'perfect-squares.html',
})
export class PerfectSquaresPage {
  intervalForm: FormGroup;
  validationMessages = {
    start: [
      {type: 'required', message: 'This field is required.'},
      {type: 'number', message: 'This field must be a number.'},
      {type: 'naturalNumber', message: 'This field must be a greater or equal than 1.'}
    ],
    end: [
      {type: 'required', message: 'This field is required'},
      {type: 'number', message: 'This field must be a number'},
      {type: 'naturalNumber', message: 'This field must be a greater or equal than 1.'}
    ],
    __all__: [
      {type: 'greaterThan', message: 'End must be greater than start.'}
    ]
  };
  results: PerfectSquare[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private perfectSquaresProvider: PerfectSquaresLocalProvider) {

    this.intervalForm = this.formBuilder.group({
      start: ['', Validators.compose([Validators.required, NaturalNumberValidator.isValid, NumberValidator.isValid])],
      end: ['', Validators.compose([Validators.required, NaturalNumberValidator.isValid, NumberValidator.isValid])]
    }, {validator: GreaterThanValidator.isValid('start', 'end')});

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfectSquaresPage');
  }

  onClickGetIntervalPerfectSquares() {
    if (this.intervalForm.valid) {
      let start = Number(this.intervalForm.value.start);
      let end = Number(this.intervalForm.value.end);
      this.perfectSquaresProvider.getIntervalPerfectSquares(start, end).subscribe((data) => {
        this.results = data;
        console.log(data);
      }, (error) => {
        console.log(error);
      });
    }
  }


}
