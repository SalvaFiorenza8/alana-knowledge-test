export class PerfectSquare {

  value: number;
  squareDivisorsSum: number;

  constructor(value, squareDivisorSum) {
    this.value = value;
    this.squareDivisorsSum = squareDivisorSum;
  }

}
