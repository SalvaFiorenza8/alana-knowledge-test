import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PerfectSquare } from '../../interfaces/perfect-square';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';

/*
  Generated class for the PerfectSquaresLocalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PerfectSquaresLocalProvider {

  constructor() {
  }

  private findDivisors(number) {
    let result = [];
    for (let i = 1; i <= Math.sqrt(number); i++) {
      if (number % i == 0) {
        result.push(i);
        if (number / i != i) {
          result.push(number / i)
        }
      }

    }
    return result;
  }

  private findSquaresSum(array) {
    let result = 0;
    for (let i = 0; i < array.length; i++) {
      result += Math.pow(array[i], 2);
    }
    return result;
  }

  private findIntervalPerfectSquares(start, end) {
    let result = [];

    for(let i = start; i <= end; i++) {
      let divisors = this.findDivisors(i);
      let divisorsSquaresSum = this.findSquaresSum(divisors);
      if (Number.isInteger(Math.sqrt(divisorsSquaresSum))) {
        let perfectSquare = new PerfectSquare(i, divisorsSquaresSum);
        result.push(perfectSquare);
      }

    }

    return result;
  }

  getIntervalPerfectSquares(start, end) {
    if (start < 0 || !Number.isInteger(start)) {
      throw new Error('start is not a natural number');
    }

    if (end < 0 || !Number.isInteger(end)) {
      throw new Error('end is not a natural number');
    }

    if (end <= start) {
      throw new Error('end must be greater than start');
    }

    return Observable.of(this.findIntervalPerfectSquares(start, end));
  }

}
