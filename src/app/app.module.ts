import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {PerfectSquaresPage} from '../pages/perfect-squares/perfect-squares';
import {PerfectSquaresLocalProvider} from '../providers/perfect-squares-local/perfect-squares-local';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PerfectSquaresPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PerfectSquaresPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PerfectSquaresLocalProvider,
  ]
})
export class AppModule {
}
